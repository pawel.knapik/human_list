package d13.ZadanieHumanList;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static Scanner scanner = new Scanner(System.in);
    public static List<Human> persons = new ArrayList<>();

    public static void main(String[] args) {
        selectFromMenu();
    }

    public static void selectFromMenu() {
        int menu;
        do {
            showMenu();
            menu = scanner.nextInt();
            switch (menu) {
                case 1:
                    addPerson();
                    break;
                case 2:
                    killPerson();
                    break;
                case 3:
                    checkIfAreEqual();
                    break;
                case 4:
                    showPersons();
                    break;
                case 0:
                    exit();
                    break;
            }
        } while (menu != 0);
    }

    public static void showMenu() {
        System.out.println();
        System.out.println("Choose the number for the action");
        System.out.println("1 - Add person");
        System.out.println("2 - Kill person");
        System.out.println("3 - Check if the persons are equal");
        System.out.println("4 - Show the persons");
        System.out.println("0 - Exit");
    }

    public static void addPerson() {
        scanner.nextLine();
        System.out.println("what is the first name of the person?");
        String firstName = scanner.nextLine();
        System.out.println("what is the last name of the person?");
        String lastName = scanner.nextLine();
        System.out.println("how old is the person?");
        int age = scanner.nextInt();
        Human human = new Human(firstName, lastName, age);
        persons.add(human);
    }

    public static void killPerson() {
        showPersons();
        System.out.println("Who should be killed! (select the number next to the person)");
        int choose = scanner.nextInt();
        persons.remove(choose - 1);
        System.out.println("He/She is dead!");
    }

    public static void checkIfAreEqual() {
        showPersons();
        System.out.println("Which persons should be check? (select the numbers next to the person)");
        int firstPerston = scanner.nextInt();
        int secondPerston = scanner.nextInt();
        if (persons.get(firstPerston - 1).equals(persons.get(secondPerston - 1))) {
            System.out.println("They are equal!");
        } else {
            System.out.println("They are not equal!");
        }
    }

    public static void showPersons() {
        for (int i = 0; i < persons.size(); i++) {
            System.out.println(i + 1 + " " + persons.get(i));
            System.out.println();
        }
    }

    public static void exit() {
        System.out.println("Good by!");
    }
}


